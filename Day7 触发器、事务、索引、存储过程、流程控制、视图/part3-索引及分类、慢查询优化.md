# Part 3 —— 索引与慢查询优化、数据库三大范式

[TOC]



## 一、索引

![索引](img\索引.png)

### 1、索引与慢查询优化

```python
索引:简单的理解为可以帮助你加快数据查询速度的工具
    也可以把索引比喻成书的目录
   
算法:解决事物的办法
    入门级算法:二分法
   	二分法前提:数据集必须有序

数据结构
	二叉树(只能分两个叉)
	b树 
    b+树(叶子节点添加了指针) 
    b*树(枝节点也添加了指针)
    # 对于精确查找(找某个数据),查找次数看深度,b，b+，b*查找次数没有区别
    # 添加指针是为了加快范围查询的速度

 
将某个字段添加成索引就相当于依据该字段建立了一颗b+树从而加快查询速度
如果某个字段没有添加索引 那么依据该字段查询数据会非常的慢(一行行查找)
```

### 2、索引的分类

```python
1.primary key
	主键索引除了有加速查询的效果之外 还具有一定的约束条件
2.unique key
	唯一键索引 除了有加速查询的效果之外 还具有一定的约束条件
3.index 
	普通索引 只有加速查询的效果 没有额外约束

	# 注意外键不是索引 它仅仅是用来创建表与表之间关系的
```

### 3、如何操作索引

![image-20220407173553508](part3-索引及分类、慢查询优化.assets/image-20220407173553508.png)

```python
创建唯一索引需要提前排查是否有重复数据
	select count(字段) from t1
  select count(distinct(字段)) from t1
查看当前表内部索引值
	show index from t1;
主键索引'primary key'
	alter table t1 add primary key pri_id(id);
唯一索引'unique key'
	alter table t1 add unique key uni_age(age)
普通索引'index'
	alter table t1 add index idx_name(name)
    前缀索引(属于普通索引)
        # 对前n个字符中查找是否包含某个数据
        避免对大列建索引,如果有就使用前缀索引
            eg:博客内容 百度搜索内容等
        alter table t1 add index idx_name(name(4))
    联合索引(属于普通索引)
            相亲平台 搜索心仪对象的时候
            # 最左匹配原则
            gender money height beautiful 
        alter table t1 add index idx_all(name,age,sex)
删除索引
	alter table t1 drop index 索引名(idx_name、idx_all...)
```

### 4、explain执行计划（查看索引扫描）

```mysql
#查看执行计划
mysql>explain <Query>;

全表扫描
	不走索引 一行行查找数据 效率极低 生产环境下尽量不要书写类似SQL
索引扫描
	走索引 加快数据查询 建议书写该类型SQL

explain就是帮助我们查看SQL语句属于那种扫描

# 常见的索引扫描类型:
	0）all	#全表扫描，性能最低

    1）index
    2）range
    3）ref
    4）eq_ref
    5）const
    6）system
    7）null
从上到下，性能从最差到最好，我们认为至少要达到range级别


# 不走索引情况(起码记忆四条及以上)
	1.没有查询条件，或者查询条件没有建立索引
    2.查询结果集是原表中的大部分数据(25％以上)
    	# 就算走索引，但是数据量太大，还是算作不走索引
        # 所以常用limit划分一下
    3.索引本身失效，统计数据不真实
    4.查询条件使用函数在索引列上或者对索引列进行运算，运算包括(+，-，*等)
    5.隐式转换导致索引失效
    	eg:字段是字符类型 查询使用整型
    6.<> ，not in 不走索引	# 使用>=时，还可以走索引
    	单独的>,<,in 有可能走，也有可能不走，和结果集有关，尽量结合业务添加limit、or或in尽量改成union
    7．like "%_" 百分号在最前面不走
    8.单独引用联合索引里非第一位置的索引列
    	# 如果创建了联合索引，但是没有使用联合索引的第一个词，所以也不走
    9.字符串不加引号
    10.where A or B ，只要A或者B中有一个没有索引，就都不走索引
    	
 
"""
索引的创建会加快数据的查询速度 但是一定程度会拖慢数据的插入和删除速度
"""
```



![image-20220407181635495](part3-索引及分类、慢查询优化.assets/image-20220407181635495.png)

![image-20220407181912242](part3-索引及分类、慢查询优化.assets/image-20220407181912242.png)

![image-20220407205314934](part3-索引及分类、慢查询优化.assets/image-20220407205314934.png)





### 5、指定索引使用

![image-20220407191249411](part3-索引及分类、慢查询优化.assets/image-20220407191249411.png)



### 6、前缀索引

![image-20220407202509236](part3-索引及分类、慢查询优化.assets/image-20220407202509236.png)



### 7、profile查询时间

```mysql
#是否支持profile
mysql> SELECT @@have_profiling;
#当前是否开启profile
mysql> SELECT @@profiling;
#开启profile（默认是关闭的）
mysql> set PROFILE=1;
#查看Query_ID及Query语句，的耗费时间
mysql> show PROFILEs;
```



### 8、B树、B+树的创建演示流程

https://www.cs.usfca.edu/~galles/visualization/Algorithms.html

innodb中的索引分为聚簇索引和非聚簇索引

聚簇索引：B+树的叶子节点(主键值)下挂的是行数据()

非聚簇索引：B+树的叶子节点(其他类型索引)下挂的是主键值(实际查的时候，会先查主键值，再通过主键值查行数据)







## 二、慢日志

```mysql
什么是MySQL慢日志?
慢日志查询的主要功能就是，记录sql语句中超过设定的时间阈值的查询语句。例如，一条查询sql语句，我们设置的阈值为1s，当这条查询语句的执行时间超过了1s，则将被写入到慢查询配置的日志中.
慢查询主要是为了我们做sql语句的优化功能.

配置项说明：
    登陆mysql服务：
    > show variables like '%query%';
    关注三个配置项即可。
  1.slow_query_log
  该配置项是决定是否开启慢日志查询功能，配置的值有ON或者OFF.
  2.slow_query_log_file
  该配置项是慢日志查询的记录文件,需要手动创建.
  3.long_query_time
  该配置项是设置慢日志查询的时间阈值，当超过这个阈值时，慢日志才会被记录.配置的值有0(任何的sql语句都记录下来)，或者>0(具体的阈值).该配置项是以秒为单位的，并且可以设置为小数.
  4.log-queries-not-using-indexes
  该配置项是为了记录未使用到索引的sql语句.
  
  
查慢日志的文件
>>find / -name *slow.log
```



>   1、将MySQL服务器中影响数据库性能的相关SQL语句记录到日志文件中
>
>   2、通过对这些特殊的SQL语句进行分析和改进，提高数据库的性能。
>
>   
>
>   默认情况下，MySQL 数据库并不启动慢查询日志，需要我们手动来设置这个参数，当然，如果不是调优需要的话，一般不建议启动该参数，因为开启慢查询日志或多或少会带来一定的性能影响。慢查询日志支持将日志记录写入文件，也支持将日志记录写入数据库表。

### 配置慢日志

```mysql
vim /etc/my.cnf

#开启慢查询
slow_query_log = on
#慢查询中记录没有使用索引的query
log_queries_not_using_indexes=on
#返回较慢的日志mysql5.6版本以上，取消了参数log-slow-queries，更改为slow-query-log-file 
slow_query_log_file=/tmp/slow.log
#慢查询时间,这里为2秒,超过2秒会被记录
long_query_time=0.05
```

### 查看是否开启

![image-20220407180329319](part3-索引及分类、慢查询优化.assets/image-20220407180329319.png)

```mysql
mysql> show variables like '%slow_query_log%';
+---------------------+---------------+
| Variable_name       | Value         |
+---------------------+---------------+
| slow_query_log      | ON            |
| slow_query_log_file | /tmp/slow.log |
+---------------------+---------------+
2 rows in set (0.00 sec)
```

### 测试慢日志

```mysql
mysql> select sleep(2);
+----------+
| sleep(2) |
+----------+
|        0 |
+----------+
1 row in set (2.00 sec)

root@756ad135dd2e:/tmp# cat slow.log 
# Time: 2021-09-28T16:15:29.967489Z
# User@Host: skip-grants user[root] @ localhost []  Id:     2
# Query_time: 2.000316  Lock_time: 0.000000 Rows_sent: 1  Rows_examined: 0
SET timestamp=1632845729;
select sleep(2);
```









## 三、数据库三大范式

```mysql
数据库三大范式？
什么是范式：简言之就是，数据库设计对数据的存储性能，还有开发人员对数据的操作都有莫大的关系。所以建立科学的，规范的的数据库是需要满足一些 规范的来优化数据数据存储方式。在关系型数据库中这些规范就可以称为范式。
什么是三大范式：
"第一范式"：当关系模式R的所有属性都不能在分解为更基本的数据单位时，称R是满足第一范式的，简记为1NF。满足第一范式是关系模式规范化的最低要求，否则，将有很多基本操作在这样的关系模式中实现不了。

"第二范式"：如果关系模式R满足第一范式，并且R得所有非主属性都完全依赖于R的每一个候选关键属性，称R满足第二范式，简记为2NF。

"第三范式"：设R是一个满足第一范式条件的关系模式，X是R的任意属性集，如果X非传递依赖于R的任意一个候选关键字，称R满足第三范式，简记为3NF.
注：关系实质上是一张二维表，其中每一行是一个元组，每一列是一个属性
```





## 四、隔离级别

```python
在SQL标准中定义了四种隔离级别，每一种级别都规定了一个事务中所做的修改
InnoDB支持所有隔离级别
	set transaction isolation level 级别

1.read uncommitted(未提交读)
	事务中的修改即使没有提交，对其他事务也都是可见的，事务可以读取未提交的数据，这一现象也称之为"脏读"
2.read committed(提交读)
	大多数数据库系统默认的隔离级别
  一个事务从开始直到提交之前所作的任何修改对其他事务都是不可见的，这种级别也叫做"不可重复读"
3.repeatable read(可重复读)		# MySQL默认隔离级别
	能够解决"脏读"问题，但是无法解决"幻读"
  所谓幻读指的是当某个事务在读取某个范围内的记录时另外一个事务又在该范围内插入了新的记录，当之前的事务再次读取该范围的记录会产生幻行，InnoDB和XtraDB通过多版本并发控制(MVCC)及间隙锁策略解决该问题
4.serializable(可串行读)
	强制事务串行执行，很少使用该级别
 
https://www.cnblogs.com/Dominic-Ji/p/15560680.html
```



