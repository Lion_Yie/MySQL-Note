# Part 2 —— 事务、流程控制

[TOC]

## 今日内容概要

```mysql
1、视图(了解)
2、触发器trigger
3、'事务'
4、存储过程procedure
5、函数
6、'流程控制'
7、函数
8、流程控制
9、索引与慢查询优化
10、索引分类

1.自行百度搜索并提炼"数据库设计三大范式"
	分别是哪三大以及各自有何特征
2.整理今日内容及博客
3.自行复习python基础
```







## 一、事务

![事务](img\事务.png)

```mysql
# 四大特性(ACID)
	A:原子性(atomicity)
        每个事务都是不可分割的最小单位(同一个事务内的多个操作要么同时成功要么同时失败)
    C:一致性(consistemcy)
        事务必须是使数据库从一个一致性状态变到另一个一致性状态。一致性与原子性是密切相关的
    I:隔离性(isolation)
        事务与事务之间彼此不干扰
    D:持久性(durablity)
        一个事务一旦提交，它对数据库中数据的改变就应该是永久性的
```

### 1、MySQL开启事务的三种方式

```mysql
1、隐式开启，隐式提交 "mysql默认"
    '默认情况下,你每敲一条SQL语句,都会开启一个事务。'
    每条sql语句一运行完，会自动帮你commit提交，所以平时我们使用update语句修改数据库，都会自动提交。

2、显式开启，显示提交"掌握"
	格式:
        start transaction;	# 或begin;
        sql语句1;
        sql语句2;
        [commint/rollback]

	# 如何开启事务
    start transaction;
    或
    begin;
    # 如何回滚
    rollback;
    # 如何确认
    commit;

3、显示开启，隐式提交'(用的少)'
    Set session autocommit =0
        这样设置完，就不会自动提交

总结：MYSQL 默认为每条sql开启事务，并且会在本条sql执行完毕后自动执行commit提交
	若想设置手动提交，有两种方式:

    方式一：直接用 SET 来改变 MySQL 的自动提交模式（下述设置均为会话级别的设置）:
    SET AUTOCOMMIT=0 禁止自动提交
    SET AUTOCOMMIT=1 开0启自动提交
    
    方式二： 手动开启的事务里默认不会自动提交
    # 手动开启的事务里默认不会自动提交，所以我们可以将要执行的sql语句放在我们自己手动开启的事务里
    start transaction;
    update test.t1 set id=33 where name = "jack";
    commit;
    注意:这种方式在当你使用commit或者rollback后，事务就结束了,再次进入事务状态需要再次start transaction
```

### 2、案例

```mysql
----------------------------------'建案例表'-----------------------------------
create table user(
	id int primary key auto_increment,
    name char(32),
    money int
)

insert into user(name,money)
values
("yly",1000),
("zpx",1000),
("aaa",1000);

----------------------------------'事务操作'-----------------------------------
start transaction;

# sql语句
update user set money=900 where name="yly";			# yly买支付100元
update user set money=1010 where name="zpx";		# 中介zpx拿走10元
update user set money=1090 where name="aaa";		# 卖家aaa拿到90元

#回滚
rollback;		# 上面三个操作，回到开启事务之前的状态
或
commit;			# 确认提交数据

注意：
使用了rollback或者commit，这个事务就结束了
如果要重新操作，那么需要重新开启事务
```



## 二、流程控制(开发了解，DBA掌握)

```mysql
注意：
	流程控制是卸载存储过程内


delimiter //
create procedure proc_if()
begin
	declare i int default 0;
	# if判断
    if i = 1 THEN
           SELECT 1;
    ELSEIF i = 2 THEN
           SELECT 2;
    ELSE
           SELECT 7;
    END IF;
END //
delimiter ;
call proc_if();


# while循环
delimiter //
create procedure proc_while()
begin
	declare num int;
	set num = 0;
	while num < 10 do
		select
			num;
		set num = num + 1;
	end while;
end //
delimiter ;
c
```



