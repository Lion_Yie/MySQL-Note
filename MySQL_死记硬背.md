# MySQL_死记硬背



## 1、索引

### 常见的索引扫描类型:(全记)

```mysql
1）index
2）range
3）ref
4）eq_ref
5）const
6）system
7）null
```
从上到下，性能从最差到最好，我们认为至少要达到range级别


### 不走索引情况(起码记忆四条及以上)
```mysql
1.没有查询条件，或者查询条件没有建立索引
2.查询结果集是原表中的大部分数据(25％以上)
3.索引本身失效，统计数据不真实
4.查询条件使用函数在索引列上或者对索引列进行运算，运算包括(+，-，*等)
5.隐式转换导致索引失效
	eg:字段是字符类型 查询使用整型
6.<> ，not in 不走索引
	单独的>,<,in 有可能走，也有可能不走，和结果集有关，尽量结合业务添加limit、or或in尽量改成union
7．like "%_" 百分号在最前面不走
8.单独引用联合索引里非第一位置的索引列
```

