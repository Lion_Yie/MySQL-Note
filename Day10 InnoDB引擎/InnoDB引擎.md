# InnoDB引擎



# 第1节	逻辑存储结构

![image-20220409000247257](InnoDB引擎.assets/image-20220409000247257.png)



![image-20220409001444054](InnoDB引擎.assets/image-20220409001444054.png)



```mysql
表空间"tablespace"(ibd文件)，一个mysql实例可以对应多个表空间，用于存储记录、索引等数据

↓

段"segment"，分为数据段(Leaf node segment)、索引段(Non-leaf node segment)、回滚段(Rollback segment),InnoDB是索引组织表,数据段就是B+树的叶子结点,索引段即为B+树的非叶子节点.段用来管理多个区。

↓

区"extent"，表空间的单元结构，每个区的大小为1M。默认情况下，InnoDB存储引擎页大小为16K，即一个区中一共有64个连续的页。

↓

页"page"，是InnoDB存储引擎磁盘管理的最小单元，每个页的大小默认为16KB。为了保证页的连续性，InnoDB存储引擎每次从磁盘申请4-5个区。

↓

行"row"，InnoDB存储引擎数据是按行进行存放的。
	Trx_id:每次对某条记录进行改动时，都会把对应的事务id赋值给trx_id隐藏列。
	Roll_pointer:每次对某条记录进行改动时，都会把旧的版本写入到undo日志中，然后这个隐藏列就相当于一个指针，可以通过它来找到该记录修改前的信息。
```









# 第2节	架构

![image-20220409003411840](InnoDB引擎.assets/image-20220409003411840.png)





## 2.1内存架构

![image-20220409003207955](InnoDB引擎.assets/image-20220409003207955.png)

```mysql
如果有一台专门的mysql服务器，那么80%的内存都会分配给mysql的缓冲区。
```



## 2.2	磁盘架构

### ①	系统表空间/独立表空间

![image-20220409003700648](InnoDB引擎.assets/image-20220409003700648.png)

```mysql
# 系统表空间
mysql> show variables like "%data_file_path%";
+-----------------------+------------------------+
| Variable_name         | Value                  |
+-----------------------+------------------------+
| innodb_data_file_path | ibdata1:12M:autoextend |	# 有一个叫ibdata1的文件，就是系统表空间
+-----------------------+------------------------+
1 row in set (0.00 sec)


# 独立表空间
mysql> show variables like "%file_per_table%";
+-----------------------+-------+
| Variable_name         | Value |
+-----------------------+-------+
| innodb_file_per_table | ON    |		# 默认都是开启的，每张表都会有一个 .ibd 的表空间文件
+-----------------------+-------+
1 row in set (0.00 sec)
```





### ②	通用表空间/undo撤销表空间

![image-20220409004624941](InnoDB引擎.assets/image-20220409004624941.png)

```mysql
# 通用表空间	(用得不多，创建出一个和系统表空间外的一个.ibd的通用表空间)
```



### ③	双写缓冲区/Redo Log

![image-20220409004934511](InnoDB引擎.assets/image-20220409004934511.png)



## 2.3	后台线程——(内存刷到磁盘)

![image-20220409121121543](InnoDB引擎.assets/image-20220409121121543.png)



### 查看IO Thread

![image-20220409121331272](InnoDB引擎.assets/image-20220409121331272.png)

```mysql
mysql>show engine innodb status;
...
...
--------
FILE I/O
--------
I/O thread 0 state: wait Windows aio (insert buffer thread)
I/O thread 1 state: wait Windows aio (log thread)
I/O thread 2 state: wait Windows aio (read thread)
I/O thread 3 state: wait Windows aio (read thread)
I/O thread 4 state: wait Windows aio (read thread)
I/O thread 5 state: wait Windows aio (read thread)
I/O thread 6 state: wait Windows aio (write thread)
I/O thread 7 state: wait Windows aio (write thread)
I/O thread 8 state: wait Windows aio (write thread)
I/O thread 9 state: wait Windows aio (write thread)
Pending normal aio reads: 0 [0, 0, 0, 0] , aio writes: 0 [0, 0, 0, 0] ,
 ibuf aio reads: 0, log i/o's: 0, sync i/o's: 0
Pending flushes (fsync) log: 0; buffer pool: 0
1370 OS file reads, 1681 OS file writes, 734 OS fsyncs
0.00 reads/s, 0 avg bytes/read, 0.00 writes/s, 0.00 fsyncs/s
...
...
```







# 第3节	事务原理

![image-20220409124414841](InnoDB引擎.assets/image-20220409124414841.png)



![image-20220409124521029](InnoDB引擎.assets/image-20220409124521029.png)

```mysql
对于事务的原子性、一致性、持久性是由innodb引擎的"redo log"和"undo log"来保证的

对于事务的隔离性是由innodb引擎的"锁机制"和"MVCC"机制来实现的
```



## 3.1	redo log重做日志--(持久性）

![image-20220409142235067](InnoDB引擎.assets/image-20220409142235067.png)

```mysql
redo log是为了保证脏页刷新磁盘错误的时候，进行数据恢复，从而保证事务的"持久性"
```





## 3.2	undo log回滚日志--(原子性)

![image-20220409161502143](InnoDB引擎.assets/image-20220409161502143.png)

```mysql
回滚日志，作用：回滚和MVCC
是逻辑日志，提供反操作(例：delete一条记录，undo log会记录一条对应的insert记录)
当执行rollback时，就可以从undo log中的逻辑记录读取响应的内容进行回滚。

undo log销毁：undo log在事务执行时产生，事务提交时，并不会立即删除undo log，因为这些日志可能要用于MVVCC
undo log存储：undo log采用段的方式进行管理和记录，存放在前面介绍的rollback segment回滚段中，内部包含1024个undo log segment
```







# 第4节	MVCC——(Multi-Version Concurrency Control)



![image-20220409165336830](InnoDB引擎.assets/image-20220409165336830.png)





## MVCC实现原理



## 4.1	隐式字段

db_trx_id

db_roll_ptr



## 4.2	undo log

![image-20220409170937236](InnoDB引擎.assets/image-20220409170937236.png)





## 4.3	undo log版本链

![image-20220409172018560](InnoDB引擎.assets/image-20220409172018560.png)

```mysql
不同事务或相同事务对同一条记录进行修改，会导致该记录的undolog生成一条记录版本的链表，链表的头部是最新的旧记录，链表尾部是最早的旧记录。
```



## 4.4	readview

### 介绍

![image-20220409174934236](InnoDB引擎.assets/image-20220409174934236.png)

### 版本链数据访问规则

![image-20220409175454504](InnoDB引擎.assets/image-20220409175454504.png)



#### 隔离级别：RC

RC隔离级别下，在事务中每一次执行快照读时生成ReadView。

##### **案例一：事务5第一次查询**

![image-20220409181223252](InnoDB引擎.assets/image-20220409181223252.png)

![image-20220409182027935](InnoDB引擎.assets/image-20220409182027935.png)

```mysql
将版本链的DB_TRX_ID带入到右边的trx_id中去
第一次,trx_id=4:
	trx_id == creator_trx_id?  	(×)
	trx_id < min_trx_id ?  		(×)
	trx_id > max_trx_id? 		(×)
	min_trx_id <= trx_id <= max_trx_id? 如果trx_id not in m_ids是可以访问该版本的。(×)
	
第二次,trx_id=3:
	trx_id == creator_trx_id?  	(×)
	trx_id < min_trx_id ?  		(×)
	trx_id > max_trx_id? 		(×)
	min_trx_id <= trx_id <= max_trx_id? 如果trx_id not in m_ids是可以访问该版本的。(×)
	
第三次,trx_id=2:
	trx_id == creator_trx_id?  	(×)
	trx_id < min_trx_id ?  		(√)	#可以访问
	# 事务5第一次查询id为30的记录结果版本为 DB_TRX_ID=2
```







##### **案例二：事务5第一次查询**

![image-20220409181257848](InnoDB引擎.assets/image-20220409181257848.png)