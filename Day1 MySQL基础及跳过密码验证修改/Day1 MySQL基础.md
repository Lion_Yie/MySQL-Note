# 数据库基础

[TOC]



## 一、基本概念

```mysql
库	>>>		相当于文件夹
表	>>>		相当于文件
记录	>>>		文件内一行行的数据

数据：
对于数据库来讲，数据就是事物的状态，例如：一个人是男是女，他的爱好是足球还是篮球居于此类。

记录：
上面一个问题我们说了，数据就是记录事物的状态。那么记录就是事物的每一个状态，这每一个记录组成了事物的状态。

表：
每一个记录组成了数据，那么存放数据的载体即为表。

库：
每一种类型的数据都存放于一个属于自己的表中，例如：同学的基本信息表，城市表，行业表等。那么不同类型的表组成了库。
```



### 数据库演变史

```mysql
# 1.文件操作阶段
	jason|123|read
   	'''每个人数据格式都不相同 程序之间无法兼容'''
# 2.软件开发目录规范
    db		数据存储文件
    '''每个人数据格式都不相同 程序之间无法兼容'''
# 3.存储位置和格式
	'''位置和格式确定 保证程序的兼容性'''
 
单机游戏
	# 各自保存各自的数据 彼此不共享
联机游戏
	# 数据存储全部统一在一个地方完成
集群的概念
	# 多台计算机从事相同的工作内容
```

### 软件开发架构

```mysql
1.c/s架构
	客户端 服务端
2.b/s架构
	浏览器 服务端
本质:bs架构本质也是cs架构
```

### SQL语句的由来

```mysql
# 什么是SQL语句?
	就是操作数据库软件的命令
# 为什么要有SQL语句?
	是因为数据库能够支持的语言非常的多 为了避免识别问题 
    发明了SQL语句 规定只要想使用数据库就统一使用SQL语句操作
```

### 数据库的本质

```mysql
数据库本质其实就是一款cs架构的软件
	所以我们所有人其实都可以写一款数据库软件
 
在市面上确实有很多数据库软件
```

### 数据库的分类

```mysql
1.关系型数据库
	# 具有固定的表结构 并且表与表之间可以通过外键建立关系
    MySQL、MariaDB、Oracle、PostgreSQL、sqlserver、sqlite、db2

2.非关系型数据库
	# 没有固定的表结构 数据存储采用K:V键值对的形式 
    redis、mongoDB、memcache
"""
我们通常将操作关系型数据库的语句称为:SQL语句
我们通常将操作非关系型数据库的语句称为:NoSQL语句
	有时候甚至使用NoSQL指代非关系型数据库 

数据库可以指代的
	1.程序
	2.数据库管理软件
	3.存放数据的某个'文件夹'
"""
```

### MySQL数据库

```mysql
# 版本问题
	MySQL5.5
    MySQL5.6  以5.6为教学依据
    MySQL5.7
    MySQL8.0
"""
不管什么版本 SQL语句都是一样的
"""

# 下载与安装
	官网 
    	点击downloads
        	点击GPL
            	选择社区版(community)
                	点击archives
             
# windows安装
	1.解压文件夹
    2.介绍:服务端(mysqld.exe)  客户端(mysql.exe)
    3.先启动服务端 之后再使用客户端链接
```



### Linux下常用端口号

```mysql
'''
常见软件的默认端口号
	MySQL		3306
    redis		6379
    mongodb		27017
    flask		5000
    tomc

'''
```





---

## 二、windows下操作

### 基本操作命令

```mysql
1.登录服务端
	mysql  # 以游客模式登录
    mysql -u用户名 -p密码 -hip -Pport  # 完整的登录命令
    mysql -u用户名 -p密码  # 本机使用该命令即可
第一次以管理员身份登录是没有密码的 直接回车即可
'''MySQL的SQL语句都是以分号作为结束符'''
2.查看当前所有的库名称
	show databases;
3.取消之前的命令
	\c  # 或者直接运行报错
4.退出客户端
	exit;
    quit;
```

### 系统服务制作

```mysql
# 1.先解决路径切换问题
	将bin目录路径添加到环境变量中 
    '''注意win7与win10区别'''
# 2.将MySQL服务制作成系统服务(开机自启动)
	1.先以管理员身份打开cmd窗口
    2.输入制作命令
    	mysqld --install
    3.启动服务(第一次需要自己启动 后面就是开机自启动)
    	1.可以鼠标右键点击启动按钮
        2.输入启动命令
        	net start mysql
    """
    如何停止服务
    	net stop mysql
   	如何移除系统服务
   		mysqld --remove
    """
```

### 修改管理员密码

```mysql
# 1.在cmd窗口内直接借助于mysqladmin命令
	# 1.1.终端直接输入修改密码的命令即可
    mysqladmin -uroot -p password 123  # 该命令输完会提示让你再输一次上一次的密码
    # 1.2.第一次修改完密码以后，之后还修改密码需要
    mysqladmin -uroot -p123 password 123456
    
# 2.在客户端中修改
	set password=PASSWORD('新密码');
```

### 忘记密码如何操作

```mysql
1.windows
# 1.关闭mysql服务
	net stop mysql  # cmd(管理员身份运行)
# 2.以跳过授权表的方式重新启动
	mysqld --skip-grant-tables	# cmd(管理员身份运行)
# 3.修改指定用户的密码(重置密码)
	update mysql.user set password=password(123) where user="root" and host="localhost";  # 新打开一个cmd(普通)修改对应用户的密码
# 4.立刻将修改数据刷到硬盘
mysql> flush privileges;	

# 5.退出客户端并且以正常的方式重新启动服务端
mysql> exit;
mysql> net start mysql

2.linux-centos7.9
[root@yly_server ~]# service mysql stop
[root@yly_server ~]# vi /etc/my.cnf
[mysqld]
skip-grant-tables
[root@yly_server ~]# service mysql start
[root@yly_server ~]# mysql
mysql> use mysql;
mysql> update mysql.user set authentication_string=password('root_password') where user='root';
mysql> flush privileges;
mysql> exit
[root@yly_server ~]# vi /etc/my.cnf	
[mysqld]
# 删掉skip-grant-tables
[root@yly_server ~]# service mysql start
[root@yly_server ~]# mysql -u root -p
```



## 添加远程连接用户

https://blog.csdn.net/shuang_waiwai/article/details/120825086

```mysql
[root@yly_server ~]# mysql -u root -p; 
mysql> use mysql;
mysql> 
mysql> 
mysql> 
mysql> 
mysql> 
[root@yly_server ~]# 
[root@yly_server ~]# 
[root@yly_server ~]# 
[root@yly_server ~]# 
```





### 字符编码

```mysql
# 1.查看编码信息
	\s
# 2.配置文件
	mysql默认的配置文件  my-default.ini
    拷贝my-default.ini文件并修改为my.ini
# 3.添加配置信息
	[mysqld]
    character-set-server=utf8
    collation-server=utf8_general_ci
    [client]
    default-character-set=utf8
    [mysql]
    default-character-set=utf8
# 4.重新服务端才能生效
```

### 重要概念

```mysql
库		文件夹
表		文件
记录      文件内一行行数据
```

