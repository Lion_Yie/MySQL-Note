# MySQL基础语句、存储引擎、数据类型

[TOC]



## 一、基础SQL语句——库、表、记录操作



### 	1、库—增删改查

```mysql
'1、增'
	create database [库名];

'2、查'
    "查所有"
    show databases;

    "查一个的详细信息"
    show create database [库名];

'3、改'
    alter databases [库名] charset="utf8";

'4、删'
    drop database [库名];
```



### 	2、表—增删改查

#### 			1、选中要操作的库

```mysql
"""
在操作表(文件)的时候  需要制定所在的库(文件夹)
"""
# 查看当前所在的库的名字
select database();
# 切换库
use [库名];
```



#### 			2、表操作

```mysql
'1.增'
    create table [表名](
        字段名1 字段类型(数字) 约束条件,
        字段名2 字段类型(数字) 约束条件,
        字段名3 字段类型(数字) 约束条件
    );
约束条件:
primary key (PK)：#标识该字段为该表的主键，可以唯一的标识记录，主键就是不为空且唯一当然其还有加速查询的作用
foreign key (FK)：#标识该字段为该表的外键，用来建立表与表的关联关系
not null：#标识该字段不能为空
unique key (UK)：#标识该字段的值是唯一的
auto_increment：#标识该字段的值自动增长（整数类型，而且为主键）
default：#为该字段设置默认值

unsigned：#将整型设置为无符号即正数
zerofill：#不够使用0进行填充


'2.查'
    # 查看当前库下面所有的表名
    show tables;
    # 指定查看某一个表(以保存格式)
    show create table [表名];
    # 指定查看某一个表(以表格形式，更适合人的审美)
    describe [表名];
    可以简写为：
    desc [表名];

'3.改'
	"改名"
	alter table [原表名] rename [新表名];
	rename table [原表名] to [新表名],
				[原表名] to [新表名],
				[原表名] to [新表名];
	
	"修改: modify  |  change "
    alter table [表名] modify name char(16);    # 将之前设置的char(4)改为char(16)
    alter table [表名] change [旧字段] [新字段] [新字段类型];# 可以改字段名，和modify差别不大。
    
    "插入字段"
    # 标准的是column可省略
    alter table [表名] add column age int;	
    
    alter table [表名] add [新字段] first		# 插入字段到第一行
	alter table [表名] add [新字段] after [原字段]# 在某字段之后插入一个新字段
    alter table [表名] add age int;			  # 末尾增加新字段age为int类型，并放在
    
    
    "删除表的字段"
    alter table [表名] drop id;				  # 删除id字段


'4.删'
    drop table [表名];
    
'5.注,操作不同库的表，表名可以写绝对路径'
    例如此时选择操作在db1的t1表内
    #进入db1的t1表内
    select database();
    use db1;
        1、想直接在db2下创建t2表。
        #以绝对路径的形式操作db2。
        create table db2.t2(id int);
        2、查看db2的t2表。
        show create table db2.t2;
        desc db2.t2;
```

##### 2.1	建表之后，再添加外键字段

![img](MySQL基础语句、存储引擎、数据类型.assets/A13AA954748185FE5FCF4F2EF053D1AB.png)

```mysql
alter table 表名 add constraint FK_ID foreign key(你的外键字段名) REFERENCES 外表表名(对应的表的主键字段名);
```





#### 3、复制表(了解)

```python
查询语句执行的结果也是一张表，可以看成虚拟表

# 复制表结构＋记录 （key不会复制: 主键、外键和索引）
create table new_service select * from service;

# 只拷贝表结构(不包含键，没有记录)
create table new1_service select * from service where 1=2;  

# 拷贝结构包含各种key，但不复制记录.
create table t4 like emp;

# 完全复制表
create table t2 like t1;
insert into t2 select * from t1;
```







### 		3、记录—增删改查

```mysql
'1、增'
 insert into 表名 values('按照字段顺序一一传值(逗号隔开)');
    insert into 表名 values(...),(...),(...);
    
'2、查'
 select * from 表名;  # 查询所有字段数据
    select 字段名1,字段名2 from 表名;  # 也可以通过库名.表名跨库
    '''如果字段过多导致展示错乱 还可以使用\G结尾分行展示'''
    
'3、改'
 update 表名 set 字段名=新值 where 字段名=存在的值  # where筛选条件
    
'4、删'
 delete from 表名 where 字段名=存在的值  # where筛选条件
-----------------------------------------------------------
#拓展：清空数据表DELETE和truncate

mysql> -- 清空test01数据表中的所有的数据
mysql> DELETE FROM test01;
Query OK, 0 rows affected (0.00 sec)

mysql> TRUNCATE TABLE test01;
Query OK, 0 rows affected (0.00 sec)

-- DELETE 和 TRUNCATE 之间的区别？

  DELETE删除的是数据，不删除索引，TRUNCATE不仅删除数据而且删除索引。

-----------------------------------------------------------

```









---

## 二、存储引擎

```python
#关键词：
1、engine	
2、InnoDB支持, and 
		'事务'	:transactions		#多条SQL语句能同时进行，能回退
    	'行级锁'	：row-level locking#某个数据\表只能被一个人访问
        '外键'	：foreign keys

"""
简单的理解为:存储引擎就是处理数据底层逻辑 不同的引擎底层处理方式有所不同
"""

# 如何查看存储引擎信息
	show engines;
   
# 需要掌握的有四个
	MyISAM
    	是MySQL5.5版本之前默认的存储引擎
        	该引擎存取数据的速度都很快
            但是安全性较低 不支持很多额外的功能
    InnoDB
    	是MySQL5.5版本之后'默认'的存储引擎
        	该引擎支持事务、行级锁、外键
            存取数据的速度没有MyISAM快但是功能和安全性更高
    memory
    	数据全部存储在内存中 速度很快但是断电立刻丢失
    blackhole
    	黑洞 任何放入其中的数据都会消失(类似于垃圾处理站)
     
# 比较存储引擎之间的差异
	'''创建表可以指定存储引擎
	create table t1(id int) engine=存储引擎;
	'''

# 不同存储引擎文件后缀名
	MyISAM
		三个文件
        	.frm	表结构
            .MYD	表数据
            .MYI	表索引
    InnoDB
		两个文件
    		.frm	表结构
        	.ibd     数据与索引
	memory
			.frm	表结构
	blackhole	
			.frm	表结构
```









## 三、数据类型

### 	1、整型

|   类型名称    | 占用字节数 |                   默认                    |   取消默认(unsigned)    |
| :-----------: | :--------: | :---------------------------------------: | :---------------------: |
|    TINYINT    |   1bytes   |                 -128〜127                 |         0 〜255         |
|   SMALLINT    |   2bytes   |               -32768〜32767               |        0〜65535         |
| INT (INTEGER) |   4bytes   |          -2147483648〜2147483647          |      0〜4294967295      |
|   not_null    |   8bytes   | -9223372036854775808〜9223372036854775807 | 0〜18446744073709551615 |



```python
案例一、验证tinyint是否会空出一个存储正负号。
    create table t6(id tinyint);
    insert into t6 values(256),(-129);
    
    select * from t6;
    显示：
    	+------+
        | id   |
        +------+
        |  127 |
        | -128 |
        +------+
        2 rows in set (0.00 sec)
# 得出结论:tinyint默认就会空出一位存储正负号
"""其实所有的int类型默认都是空出一位存储正负号"""
    
案例二、取消默认(unsigned)后的，tinyint是否还会空出一个储存正负号。
    create table t7(id tinyint unsigned);
    insert into t7 values(256),(-129);

    select * from t7;
    显示:
        +------+
        | id   |
        +------+
        |    0 |
        |  255 |
        +------+
        2 rows in set (0.00 sec)
# 得出结论:tinyint取消默认就不会空出一位存储正负号
```

### 	2、浮点型

```python
float(255,30)
	总共255位小数位占30位
double(255,30)
	总共255位小数位占30位
decimal(65,30)
	总共65位小数占30位
# 一般情况下，使用float足够了，越精确，消耗的资源越大。
----------------------------------------------------------
案例：创建三种浮点型表，插入相同位数的小数，从而对比精度。插入'1.1111111111111111111111111111'
	
create table t8(id float(255,30));
create table t9(id double(255,30));
create table t10(id decimal(65,30));
insert into t8 values(1.1111111111111111111111111111);
insert into t9 values(1.1111111111111111111111111111);
insert into t10 values(1.1111111111111111111111111111);

mysql> select * from t8;
    +----------------------------------+
    | id                               |
    +----------------------------------+
    | 1.111111164093017600000000000000 |
    +----------------------------------+
    1 row in set (0.00 sec)

mysql> select * from t9;
    +----------------------------------+
    | id                               |
    +----------------------------------+
    | 1.111111111111111200000000000000 |
    +----------------------------------+
    1 row in set (0.00 sec)

mysql> select * from t10;
    +----------------------------------+
    | id                               |
    +----------------------------------+
    | 1.111111111111111111111111111100 |
    +----------------------------------+
    1 row in set (0.00 sec)


    '''
   三者精确度不同
   decimal > double > float
   具体使用结合实际情况
    '''
```



### 	3、字符类型

```python
char(4)  	'定长'
	最多存储四个字符 超出了则报错 没超出则用空格填充
    #存取速度快，但是占用空间
    
varchar(4)  '变长'
	最多存储四个字符 超出了则报错 没超出有几个存几个
    #更省硬盘空间，每次存取数据需要先考虑报头，存取速度相对慢
----------------------------------------------------------
案例：char和varchar的区别:
    create table t11(id int,name char(4));
    create table t12(id int,name varchar(4));
    insert into t11 values(1,'jason');
    insert into t12 values(1,'jason');
```



### 4、日期类型

```mysql
date		年月日			#格式:2000-11-11
time		分时秒 		#格式:11:11:11
datetime	年月日 分时秒	  #格式:2000-11-11 11:11:11
year		年			 #格式:2000

create table user(
	id int comment '编号',
    name varchar(255) comment '姓名',
    reg_time datetime comment '注册时间',
    birthday date comment '生日',
    study_time time comment '学习时间',
    born_year year comment '年份'
);
"""
字段也可以加类似于注释的说明
	comment	后面是空格，不是等于号
"""
# 此处先人工智能模拟 
insert into user values(1,'jason','2000-11-11 11:11:11','2000-11-11','11:11:11','2000');

```





### 5、枚举与集合

```mysql
枚举：(多选一，'必须选择一个')
是单独的数据类型，不能和其他数据类型混在一起用。
enumerate		英 [ɪˈnjuːməreɪt]   美 [ɪˈnuːməreɪt]  
                vt.列举;枚举

    格式：
    	[字段名] enum("","","")

案例一：创建一个收集同学的性别表，性别字段提前设置好。
use db1;
create table classmate1(
	id int primary key auto_increment,
    name varchar(32),
    gender enum("male","female")
);

插入记录:
insert into classmate1(name,gender) values("Yie","male"),("zpx","female");	#成功
insert into classmate1(name,gender) values("aaa","other");		#性别为other，报错
插入空
insert into classmate1(name,gender) values("bbb");				#报错
insert into classmate1(name,gender) values("bbb","");			#报错


----------------------------------------
集合：(多选多，'可以什么都不选，但是那一项不能不填')
	格式：
		[字段名] set("","","")

案例二：创建一个同学的兴趣爱好表，兴趣爱好可以多选多。
use db1;
create table classmate2(
	id int primary key auto_increment,
    name varchar(32),
    hobby set("basketball","football","tennis")
);

插入记录:
insert into classmate2(name,hobby) values("Yie","basketball,football,tennis");	
insert into classmate2(name,hobby) values("aaa","other");		#报错，要在集合内选
插入空置:
insert into classmate2(name,hobby) values("aaa","");		#成功
insert into classmate2(name,hobby) values("aaa");			#报错
```





## 四、基本配置

```python
mysql目前要检查的配置有2点
    1、字符编码
    2、SQL_MODE

1、三个客户端的字符编码
    [root@web ~]# vim my.ini
    [mysqld]
    character-set-server=utf8
    collation-server=utf8_general_ci
    [client]
    default-character-set=utf8
    [mysql]
    default-character-set=utf8

2、修改sql_mode为(插入或更新数据时进行更严格的检查)
    show variables like '%mode%';
    set global sql_mode = 'strict_trans_tables,pad_char_to_full_length';
    
    #设置分组之后，只能显示组信息
	set global sql_mode='only_full_group_by';
    重新启动客户端生效。
```







