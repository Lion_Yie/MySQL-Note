# 模块pymysql操作mysql

[TOC]



## 一、安装pymysql

```python
下载模块
	1.命令行
    	pip3 install pymysql
		pip3 install pymysql -i 源地址
    2.借助于pycharm
    3.python解释器配置文件
```



## 二、模块使用

### 1、基本介绍

```python
"""
最基础为：
    1、创建连接
    2、创建游标对象
    3、定义SQL语句
    4、执行SQL语句
    5、获取返回结果
"""

import pymysql
# 创建连接
conn = pymysql.connect(
	host="localhost",
    port=3306,
    user="yly",
    password="123",
    database="db_7",
    charset="utf8",
    # autocommit=True		#这个
)
# 创建游标对象(就相当于进入了cmd中mysql客户端)	cursor : n.(计算机荧光屏上的)光标，游标
cursor = conn.cursor()		#执行完毕返回的结果集默认以元组显示

# 定义SQL语句
sql = "select * from userinfo"

# 执行SQL语句
cursor.execute(sql,args)

# 获取返回结果
res = cursor.fetchall()
print(res)
      
# 在后面不需要使用数据的时候，主动关闭链接 释放资源
cursor.close()
conn.close()	# 也可不写，python会在程序运行结束之后自动关
```

### 2、指定cursor游标对象的输出格式

```python
cursor = conn.cursor()		#执行完毕返回的结果集默认以元组显示，不显示字段名

cursor = conn.cursor(cursor=pymysql.cursors.DictCursor)	# 指定字典输出，为  "字段名"：value
							#注意是cursors
```

### 3、cursor方法一：execute执行

#### 	3.1    sql注入问题

```python
"""
结论:别自己传参，使用pymysql的execute
"""

import pymysql

# 创建链接
conn = pymysql.connect(
    host='127.0.0.1',
    port=3306,
    user='root',
    password='123',
    database='db_5',
    charset='utf8'
)
# 生成一个游标对象
cursor = conn.cursor(cursor=pymysql.cursors.DictCursor)  # 让数据自动组织成字典
# 获取用户名和密码
username = input('username>>>:').strip()
password = input('password>>>:').strip()
# 构造SQL语句
# sql = "select * from userinfo where name='%s' and password='%s'"%(username,password)
# 针对核心数据 不要自己拼接 交由execute方法帮你筛选再拼接
sql = "select * from userinfo where name=%s and password=%s"
print(sql)
# 执行SQL语句
cursor.execute(sql,(username,password))
res = cursor.fetchall()
if res:
    print(res)
    print('登录成功')
else:
    print('用户名或密码错误')
"""
登录功能
    1.获取用户名和密码
    2.基于用户名和密码直接精准查找
"""
"""
问题1 
    用户名正确 不需要密码也能登录
问题2
    用户名和密码都不需要也能登录

SQL注入问题的产生
    就是通过一些特殊符号的组合 达到某些特定的效果从而避免常规的逻辑
SQL注入问题如何解决
    execute方法自动帮你解决
"""
```

#### 		3.2	execute用法

```python
cursor.execute(query,args)			# query为sql语句，args为传给sql语句的参数
--------------------------------"案例"-------------------------------------
# 情况1、执行sql语句，不需传参:
sql="select * from db_7;"
cursor.execute(sql)
	
# 情况2、执行sql语句，需传一个参:
sql="select * from db_7 where id = %s;"
cursor.execute(sql,(1,))			# args只传一个参，需要加逗号

# 情况3、执行sql语句，需传多个参：
sql="select * from db_7 where name='%s' and password='%s';"
cursor.execute(sql,("yly","123"))

--------------------------------------------------------------------------
"""
execute的返回值:
print(cursor.execute(sql))		# 返回值是执行SQL语句之后，受影响的行数
"""
```

### 4、conn的方法一:commit	确认操作数据库

```python
"""
查找
"""

import pymysql

# 创建链接
conn = pymysql.connect(
    host='127.0.0.1',
    port=3306,
    user='root',
    password='123',
    database='db_5',
    charset='utf8',
    autocommit=True  # 涉及到增删改 自动二次确认
)
# 生成一个游标对象
cursor = conn.cursor(cursor=pymysql.cursors.DictCursor)  # 让数据自动组织成字典
sql1 = 'select * from userinfo'
sql2 = 'insert into userinfo(name,password) values(%s,%s)'
sql3 = 'update userinfo set name="jasonNB" where id=1'
sql4 = 'delete from userinfo where id=2'

# 1.查询语句可以正常执行并获取结果
# cursor.execute(sql1)
# 2.插入语句能够执行 但是并没有影响表数据
# cursor.execute(sql2,('jackson',666))
# 3.更新语句能够执行 但是并没有影响表数据
# res = cursor.execute(sql3)
# print(res)
# 4.删除语句能够执行 但是并没有影响表数据
# res = cursor.execute(sql4)
# print(res)

'''针对增删改操作 需要二次确认才可生效'''
# cursor.execute(sql2,('jackson',666))
# conn.commit()
# cursor.execute(sql3)
# conn.commit()
# cursor.execute(sql4)
# conn.commit()

cursor.executemany(sql2,[('jason111',123),('jason222',321),('jason333',222)])

# 主动关闭链接 释放资源
# conn.close()
```





### 5、cursor方法二：fetch批量查询

```python
'''
分为三种：
	cursor.fetchall()
    cursor.fetchone()
    cursor.fetchmany()
'''
1、fetchone					
	# 以光标所在位置，往后取出一个结果
    res=cursor.fetchone()
    print(res)
2、fetchmany					
	# 以光标所在位置，往后取出多个结果
    res=cursor.fetchmany(2)
    print(res)
3、fetchall					
	# 以光标所在位置，往后取出剩余所有结果
    res=cursor.fetchall()
    print(res)
```

### 6、cursor方法三：scroll 移动光标

```python
cursor.scroll(3,mode='absolute') # 相对绝对位置移动

cursor.scroll(3,mode='relative') # 相对当前位置移动
```

